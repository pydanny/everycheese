EveryCheese
=============================

The Ultimate Cheese Index!


Warning!
========
This is a test project for use in writing `Django Crash Course`_. It is NOT intended as a reference. It probably won't match what's in the book. This repo can be deleted or changed at any time.

_`Django Crash Course`: https://www.roygreenfeld.com/products/django-crash-course
